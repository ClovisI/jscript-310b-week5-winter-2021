// Create a new <a> element containing the text "Buy Now!"
// with an id of "cta" after the last <p>
const main = document.getElementsByTagName("main")[0];
const newA = document.createElement("a");
const text = document.createTextNode("Buy Now!");
newA.appendChild(text);
newA.setAttribute("id", "cta");
main.appendChild(newA);

// Access (read) the data-color attribute of the <img>,
// log to the console
const dataColor = document.getElementsByTagName("img")[0].getAttribute("data-color");
console.log(dataColor);

// Update the third <li> item ("Turbocharged"),
// set the class name to "highlight"
const liEls = document.getElementsByTagName('li');
liEls[2].className = liEls[2].className + 'highlight';


// Remove (delete) the last paragraph
// (starts with "Available for purchase now…")
let removePara = document.getElementsByTagName('p')[0];
removePara.remove();
