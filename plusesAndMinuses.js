// When a user clicks the + element, the count should increase by 1 on screen.

// When a user clicks the – element, the count should decrease by 1 on screen.

let counter = 0;
const plus = document.getElementById('plus');
const minus = document.getElementById('minus');

plus.addEventListener('click', function(){
  counter++;
  document.getElementById('counter').innerHTML = counter;
})

minus.addEventListener('click', function(){
  counter--;
  document.getElementById('counter').innerHTML = counter;
})
