// Change the text of the "Seattle Weather" header to "February 10 Weather Forecast, Seattle"
const changeText = document.getElementsByTagName("h2")[0];
changeText.innerHTML="February 10 Weather Forecast, Seattle";

// Change the styling of every element with class "sun" to set the color to "orange"
const suns =document.getElementsByClassName("sun");
for (var i = 0; i < suns.length; i++) {
  suns[i].style.color="orange";
}

// Change the class of the second <li> from to "sun" to "cloudy"
let liTwo = document.getElementsByTagName("li")[2].className="cloudy";
